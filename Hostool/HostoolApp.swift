//
//  HostoolApp.swift
//  Hostool
//
//  Created by Beyoug on 2024/11/7.
//

import SwiftData
import SwiftUI

@main
struct HostoolApp: App {
    @StateObject private var hostsManager = HostsManager()
        
        var body: some Scene {
            WindowGroup {
                ContentView()
                    .environmentObject(hostsManager)
            }
        }
}
